#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

void inicializar(void);
using namespace std;
int W=800, H=600;
float aspect = (float)W/(float)H;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;

int contador = 600;

float cameraAngleX = 0;
float cameraAngleY = 0;

float clipAreaL,
      clipAreaR,
      clipAreaT,
      clipAreaB;

float speed = 0.01;


GLUquadricObj *m_Sphere;


void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        aspect = (float)W/(float)H;

        if(W >= H)
        {
                clipAreaL = -1.0 * aspect;
                clipAreaR = 1.0 * aspect;
                clipAreaB = -1.0;
                clipAreaT = 1.0;
        }
        else
        {
                clipAreaL = -1.0;
                clipAreaR = 1.0;
                clipAreaB = -1.0 / aspect;
                clipAreaT = 1.0 / aspect;
        }
        gluOrtho2D(clipAreaL, clipAreaR, clipAreaB, clipAreaT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glEnable(GL_LIGHT1);
        glEnable(GL_LIGHT2);
        glEnable(GL_LIGHT3);

        m_Sphere = gluNewQuadric();

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        glutPostRedisplay();
}

unsigned char letraA[]= {
        0xC0, 0x03,
        0xC0, 0x03,
        0xC0, 0x03,
        0xC0, 0x03,
        0xC0, 0x03,
        0xDF, 0xFB,
        0x7F, 0XFE,
        0X60, 0X06,
        0X30, 0X0C,
        0X30, 0X0C,
        0X18, 0X18,
        0X18, 0X18,
        0x0C, 0x30,
        0x0C, 0x30,
        0X07, 0XE0,
        0X07, 0XE0
};

unsigned char letraC[]= {
        0x1F, 0xFF,
        0x3F, 0xFF,
        0x7D, 0xFF,
        0xFF, 0xFE,
        0xF8, 0x00,
        0xFC, 0x00,
        0xFC, 0X00,
        0XFC, 0X00,
        0XFC, 0X00,
        0XFC, 0X00,
        0XFC, 0X00,
        0xF8, 0x00,
        0xFF, 0xFE,
        0x7D, 0xFF,
        0x3F, 0xFF,
        0x1F, 0xFF
};

unsigned char letraR[]= {
        0X70, 0X1E,
        0X70, 0X3E,
        0x70, 0x7C,
        0x70, 0xF8,
        0X70, 0XF0,
        0X7F, 0XE0,
        0X7F, 0XC0,
        0X7F, 0XFC,
        0X70, 0XFE,
        0x70, 0X0F,
        0x70, 0x07,
        0x70, 0x07,
        0x70, 0x07,
        0x70, 0x0F,
        0x7F, 0xFE,
        0x3F, 0xFC
};

unsigned char letraL[]= {
        0x3F, 0xFC,
        0x7F, 0xFC,
        0x7F, 0xF8,
        0x78, 0x00,
        0x78, 0x00,
        0x78, 0x00,
        0x78, 0X00,
        0X78, 0X00,
        0X78, 0X00,
        0X78, 0X00,
        0X78, 0X00,
        0X78, 0X00,
        0x78, 0x00,
        0x78, 0x00,
        0X78, 0X00,
        0X78, 0X00
};

unsigned char letraO[]= {
        0x1F, 0xF8,
        0x3F, 0xFC,
        0x7F, 0xFE,
        0xF0, 0x0F,
        0xE0, 0x07,
        0xE0, 0x07,
        0xE0, 0x07,
        0xE0, 0x07,
        0xE0, 0x07,
        0xE0, 0x07,
        0xE0, 0x07,
        0xE0, 0x07,
        0xF0, 0x0F,
        0x7F, 0xFE,
        0x3F, 0xFC,
        0x1F, 0xF8
};

unsigned char letraS[]= {
        0x7F, 0xF8,
        0xFF, 0xFC,
        0xFF, 0xFE,
        0x00, 0x07,
        0x00, 0x07,
        0x00, 0x07,
        0x3F, 0xFF,
        0x3F, 0XFE,
        0X3F, 0XFC,
        0XE0, 0X00,
        0XE0, 0X00,
        0XE0, 0X00,
        0XE0, 0X00,
        0X7F, 0XFF,
        0x3F, 0xFF,
        0x1F, 0xFE
};

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        glPushMatrix();
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2f(-.6, 0.0);
        glBitmap(16, 16, 0.0, 0.0, 0.0, 0.0, letraC);
        glPopMatrix();

        glPushMatrix();
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2f(-0.4, 0.0);
        glBitmap(16, 16, 0.0, 0.0, 0.0, 0.0, letraA);
        glPopMatrix();

        glPushMatrix();
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2f(-0.2, 0.0);
        glBitmap(16, 16, 0.0, 0.0, 0.0, 0.0, letraR);
        glPopMatrix();

        glPushMatrix();
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2f(0.0, 0.0);
        glBitmap(16, 16, 0.0, 0.0, 0.0, 0.0, letraL);
        glPopMatrix();

        glPushMatrix();
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2f(0.2, 0.0);
        glBitmap(16, 16, 0.0, 0.0, 0.0, 0.0, letraO);
        glPopMatrix();

        glPushMatrix();
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glColor3f(1.0, 1.0, 1.0);
        glRasterPos2f(0.4, 0.0);
        glBitmap(16, 16, 0.0, 0.0, 0.0, 0.0, letraS);
        glPopMatrix();
        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'w':
        case 'W': cameraAngleX--; break;

        case 's':
        case 'S': cameraAngleX++; break;

        case 'a':
        case 'A': cameraAngleY--; break;

        case 'd':
        case 'D': cameraAngleY++; break;

                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:

                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        //Free memory
        if(m_Sphere) gluDeleteQuadric(m_Sphere);
        m_Sphere = NULL;

        return 0;
}

