#include <iostream>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL

void inicializar(void);
using namespace std;
int W=400, H=400;
float aumentox = 0.0;
float aumentoy = 0.0;

float aumentox2 = 0.4;
float aumentoy2 = 0.5;

float aumentox3 = 0.2;
float aumentoy3 = 0.2;

float distancia3 = 0.0;
float distancia2 = 0.0;
float distancia = 0.0;

float distancial3 = 0.0;
float distancial2 = 0.0;
float distancial = 0.0;

float ballXmin;
float ballXmax;
float ballYmin;
float ballYmax;
float ballRadius = 0.14;
int cambiox = 1;
int cambioy = 1;

int cambiox2 = 1;
int cambioy2 = 1;

int cambiox3 = 1;
int cambioy3 = 1;

bool toque1 = false;
bool toque2 = false;
bool toque3 = false;

float clipAreaXLeft;
float clipAreaXRight;
float clipAreaYBottom;
float clipAreaYTop;

float mousex;
float mousey;

float puntox;
float puntoy;


float puntox2;
float puntoy2;

int click = 0;

float vectora1 = 0 ;
float vectorb1 = 0 ;
float vectorc1 = 0 ;
float vectord1 = 0 ;

float vectora2 = 0 ;
float vectorb2 = 0 ;
float vectorc2 = 0 ;
float vectord2 = 0 ;

float vectora3 = 0 ;
float vectorb3 = 0 ;
float vectorc3 = 0 ;
float vectord3 = 0 ;

float punto =0;
float sq = 0;
float limite =0;

float punto2 =0;
float sq2 = 0;
float limite2 =0;

float punto3 =0;
float sq3 = 0;
float limite3 =0;

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        //glOrtho(-1.0, 1.0, -1.0, 1.0, 1.0, -1.0);
        float aspect= (float)W / (float)H;
        if (W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        } else {
                clipAreaXLeft = -1.0;
                clipAreaXRight = 1.0;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight,clipAreaYBottom, clipAreaYTop);
        ballXmin = clipAreaXLeft + ballRadius;
        ballXmax = clipAreaXRight - ballRadius;
        ballYmin = clipAreaYBottom + ballRadius;
        ballYmax = clipAreaYTop - ballRadius;

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
}

void Idle(void)
{
        glutPostRedisplay();
        aumentox += 0.015 * cambiox;
        aumentoy += 0.015 * cambioy;

        aumentox2 += 0.02 * cambiox2;
        aumentoy2 += 0.02 * cambioy2;

        aumentox3 += 0.018 * cambiox3;
        aumentoy3 += 0.018 * cambioy3;

        if (aumentox >= ballXmax) {
                cambiox = -cambiox;
                // izquierda
        }
        if (aumentox <= ballXmin) {
                cambiox = 1;
                // derecha
        }
        if (aumentoy >= ballYmax) {
                cambioy = -cambioy;
                // abajo
        }
        if (aumentoy <= ballYmin) {
                cambioy = 1;
                // arriba
        }

        if (aumentox2 >= ballXmax) {
                cambiox2 = -cambiox2;
        }
        if (aumentox2 <= ballXmin) {
                cambiox2 = 1;
        }
        if (aumentoy2 >= ballYmax) {
                cambioy2 = -cambioy2;
        }
        if (aumentoy2 <= ballYmin) {
                cambioy2 = 1;
        }


        if (aumentox3 >= ballXmax) {
                cambiox3 = -cambiox3;
        }
        if (aumentox3 <= ballXmin) {
                cambiox3 = 1;
        }
        if (aumentoy3 >= ballYmax) {
                cambioy3 = -cambioy3;
        }
        if (aumentoy3 <= ballYmin) {
                cambioy3 = 1;
        }

        // Circulo verde con blanco
        distancia = sqrt( pow( aumentox2 - aumentox, 2 ) + pow( aumentoy2 - aumentoy, 2 ) );
        if (distancia > ballRadius + ballRadius ) {
                toque1=false;
        } else if (distancia <= ballRadius + ballRadius) {
                toque1=true;
                if (cambiox != cambiox2) {
                        cambiox= -cambiox;
                        cambiox2= -cambiox2;
                }
                if (cambioy != cambioy2) {
                        cambioy= -cambioy;
                        cambioy2= -cambioy2;
                }
        }


        // Circulo blanco con rojo
        distancia2 = sqrt( pow( aumentox2 - aumentox3, 2 ) + pow( aumentoy2 - aumentoy3, 2 ) );
        if (distancia2 > ballRadius + ballRadius ) {
                toque2 = false;
        } else if (distancia2 <= ballRadius + ballRadius) {
                toque2 = true;
                if (cambiox3 != cambiox2) {
                        cambiox3= -cambiox3;
                        cambiox2= -cambiox2;
                }
                if (cambioy3 != cambioy2) {
                        cambioy3= -cambioy3;
                        cambioy2= -cambioy2;
                }
        }

        // Circulo blanco con rojo
        distancia3 = sqrt( pow( aumentox3 - aumentox, 2 ) + pow( aumentoy3 - aumentoy, 2 ) );
        if (distancia3 > ballRadius + ballRadius ) {
                toque3 = false;
        } else if (distancia3 <= ballRadius + ballRadius) {
                toque3 = true;
                if (cambiox3 != cambiox) {
                        cambiox3= -cambiox3;
                        cambiox= -cambiox;
                }
                if (cambioy3 != cambioy) {
                        cambioy3= -cambioy3;
                        cambioy= -cambioy;
                }
        }

        if (click > 0 && click%2 != 0) {
          puntox = mousex;
          puntoy = mousey;
        }
        if (click%2 == 0 && click >0) {
          puntox2 = mousex;
          puntoy2 = mousey;
        }

        //Distancia linea verde
        vectora1 = aumentox - puntox;
        vectorb1 = aumentoy - puntoy;
        vectorc1 = puntox2 - puntox;
        vectord1 = puntoy2 - puntoy;

        punto = vectora1 * vectorc1 + vectorb1 * vectord1;
        sq = vectorc1 * vectorc1 + vectord1 * vectord1;
        limite = punto / sq;

        distancial = abs(vectora1 * vectord1 - vectorc1 * vectorb1) / sqrt(sq);
          if (distancial < 0.15 && limite < 1 && limite > 0) {
            cambiox = -cambiox;
            cambioy = -cambioy;
          }



        //Distancia linea blanco
        vectora2 = aumentox2 - puntox;
        vectorb2 = aumentoy2 - puntoy;
        vectorc2 = puntox2 - puntox;
        vectord2 = puntoy2 - puntoy;

        punto2 = vectora2 * vectorc2 + vectorb2 * vectord2;
        sq2 = vectorc2 * vectorc2 + vectord2 * vectord2;
        limite2 = punto2 / sq2;

        distancial2 = abs(vectora2 * vectord2 - vectorc2 * vectorb2) / sqrt(vectorc2 * vectorc2 + vectord2 * vectord2);
        if (distancial2 < 0.15 && limite2 < 1 && limite2 > 0) {
          cambiox2 = -cambiox2;
          cambioy2 = -cambioy2;
        }


        //Distancia linea rojo
        vectora3 = aumentox3 - puntox;
        vectorb3 = aumentoy3 - puntoy;
        vectorc3 = puntox2 - puntox;
        vectord3 = puntoy2 - puntoy;

        punto3 = vectora3 * vectorc3 + vectorb3 * vectord3;
        sq3 = vectorc3 * vectorc3 + vectord3 * vectord3;
        limite3 = punto3 / sq3;

        distancial3 = abs(vectora3 * vectord3 - vectorc3 * vectorb3) / sqrt(vectorc3 * vectorc3 + vectord3 * vectord3);
        if (distancial3 < 0.15 && limite3 < 1 && limite3 > 0) {
          cambiox3 = -cambiox3;
          cambioy3 = -cambioy3;
        }


}

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        // verde
        glPushMatrix();
        glTranslatef(aumentox, aumentoy, 0.0);
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(0.0, 1.0, 0.0);
        glVertex2f (0.0, 0.0);
        int segmentos = 100;
        float angulo;
        float pi = 3.1416;
        for (int i = 0; i <= segmentos; i++) {
                angulo = i * 2.0 * pi / segmentos;
                glVertex2f(cos(angulo) * ballRadius, sin(angulo) * ballRadius);
        }
        glEnd();
        glPopMatrix();

        // Circulo blanco
        glPushMatrix();
        glTranslatef(aumentox2, aumentoy2, 0.0);
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(1.0, 1.0, 1.0);
        glVertex2f (0.0, 0.0);
        for (int i = 0; i <= segmentos; i++) {
                angulo = i * 2.0 * pi / segmentos;
                glVertex2f(cos(angulo) * ballRadius, sin(angulo) * ballRadius);
        }
        glEnd();
        glPopMatrix();

        // Circulo Rojo
        glPushMatrix();
        glTranslatef(aumentox3, aumentoy3, 0.0);
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(1.0, 0.0, 0.0);
        glVertex2f (0.0, 0.0);
        for (int i = 0; i <= segmentos; i++) {
                angulo = i * 2.0 * pi / segmentos;
                glVertex2f(cos(angulo) * ballRadius, sin(angulo) * ballRadius);
        }
        glEnd();
        glPopMatrix();

        //Linea
if (click > 0 && click%2==0 ){
          glPushMatrix();
          glLineWidth(4.5);
          glColor3f(1.0, 1.0, 1.0);
          glBegin(GL_LINES);
          glVertex3f(puntox, puntoy, 0.0);
          glVertex3f(puntox2, puntoy2, 0.0);
          glEnd();
          glPopMatrix();
}


        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                //que hace con a
                break;
        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        mousex = ((float)x/W) * 2 - 1;
                        mousey = -((float)y/H) * 2 + 1;
                        click ++;
                        break;
                case GLUT_RIGHT_BUTTON:
                        puntox = 0;
                        puntoy = 0;
                        puntox2 = 0;
                        puntoy2 = 0;
                        click = 0;
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        return 0;
}
