#include <iostream>
#include <stdio.h>
#include <time.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL
#include <math.h>

void inicializar(void);
using namespace std;
int W=800, H=600;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;
float C = 100;
float PI = 3.141519265;

float clipAreaXLeft = 0;
float clipAreaXRight = 0;
float clipAreaYBottom = 0;
float clipAreaYTop = 0;
float ballRadius = 0.4f;
int segundos = 0;
int minutos = 0;
int horas = 0;

void NewAspect(){
        float aspect = (float)W / (float)H;
        if(W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        }
        else{
                clipAreaXLeft = -1.0f;
                clipAreaXRight = 1.0f;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop);
}

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        //gluPerspective(40.0f, (GLfloat)W/(GLfloat)H, 1.0f, 1000.0f);
        NewAspect();

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST); //Profundidad

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        TimeStart = glutGet(GLUT_ELAPSED_TIME);

        C -= deltaTime;

        if(C<=0) {
                C=1000;
                segundos+=1;

                if(segundos % 60 == 0) {
                        minutos+=1;

                        if(minutos % 60 == 0) {
                                horas+=1;
                        }
                }
        }

        glutPostRedisplay();
}

void render(void)
{

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        glPointSize(1.0f);
        glBegin(GL_POINTS);
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(0.0f, 0.0f);
        int segmentos = 60;
        float angulo;
        for(int i = 0; i <= segmentos; i++) {
                angulo = i * 2.0f * PI / segmentos;
                glVertex2f(cos(angulo) * ballRadius, sin(angulo) * ballRadius);
        }
        glEnd();

        glColor3f(0.0f, 1.0f, 1.0f);

        glBegin(GL_LINES);
        angulo = (segundos * -1) * 2.0f * PI / segmentos;
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cos(angulo) * ballRadius, sin(angulo) * ballRadius);
        glEnd();

        glColor3f(1.0f, 1.0f, 0.0f);

        glBegin(GL_LINES);
        angulo = (minutos * -1) * 2.0f * PI / segmentos;
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cos(angulo) * (ballRadius - 0.10f), sin(angulo) * (ballRadius - 0.10f));
        glEnd();

        glColor3f(1.0f, 1.0f, 1.0f);
        glBegin(GL_LINES);
        angulo = (horas * -1) * 2.0f * PI / segmentos;
        glVertex2f(0.0f, 0.0f);
        glVertex2f(cos(angulo) * (ballRadius - 0.20f), sin(angulo) * (ballRadius - 0.20f));
        glEnd();

        glutSwapBuffers();

        deltaTime = TimeStart - TimeOld;
        TimeOld = TimeStart;
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                segundos+=1;
                break;

        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':

                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        return 0;
}
