#include <iostream>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL

void inicializar(void);
using namespace std;
int W=400, H=400;
float aumentox =0.0;
float aumentoy =0.0;
float ballXmin;
float ballXmax;
float ballYmin;
float ballYmax;
float ballRadius = 0.2;
int cambiox = 1;
int cambioy = 1;
float color1 = 1.0;
float color2 = 0.0;
float color3 = 0.0;

float clipAreaXLeft ;
float clipAreaXRight ;
float clipAreaYBottom ;
float clipAreaYTop;

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        //glOrtho(-1.0, 1.0, -1.0, 1.0, 1.0, -1.0);
        float aspect= (float)W / (float)H;
        if (W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        } else {
                clipAreaXLeft = -1.0;
                clipAreaXRight = 1.0;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight,clipAreaYBottom, clipAreaYTop);
        ballXmin = clipAreaXLeft + ballRadius;
        ballXmax = clipAreaXRight - ballRadius;
        ballYmin = clipAreaYBottom + ballRadius;
        ballYmax = clipAreaYTop - ballRadius;

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
}

void Idle(void)
{
        glutPostRedisplay();
        aumentox += 0.008 * cambiox;
        aumentoy += 0.008 * cambioy;
        if (aumentox >= ballXmax) {
          cambiox = -cambiox;
          color2 = 1.0;
        }
        if (aumentox <= ballXmin) {
          cambiox = 1;
          color2 = 0.0;
          color3= 1.0;
        }
        if (aumentoy >= ballYmax) {
          cambioy = -cambioy;
          color3= 0.0;
          color1=1.0;
        }
        if (aumentoy <= ballYmin) {
          cambioy = 1;
          color2 = 1.0;
          color1 = 0.0;
        }
}

void render(void)
{      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glLoadIdentity();
        glTranslatef(aumentox, aumentoy, 0.0);
        glBegin(GL_TRIANGLE_FAN);
        glColor3f(color1, color2, color3);
        glVertex2f (0.0, 0.0);
        int segmentos = 100;
        float direccion;
        float angulo;
        float pi = 3.1416;
        for (int i = 0; i <= segmentos; i++) {
          angulo = i * 2.0 * pi / segmentos;
          glVertex2f(cos(angulo) * ballRadius, sin(angulo) * ballRadius);
        }
        glEnd();
        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                //que hace con a
                break;
        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        return 0;
}
