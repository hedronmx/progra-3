#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL

void inicializar(void);
using namespace std;
int W=800, H=600;
float aspect = (float)W/(float)H;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;

int counter = 1000;

float cameraAngleX = 0;
float cameraAngleY = 0;

//Viewport
float   clipAreaL,
        clipAreaR,
        clipAreaT,
        clipAreaB;

//Lights
float speed = 0.01;

float ambientColor[] = {0.2f, 0.2f, 0.2f, 1.0f};
float diffuseColor[] = {0.8f, 0.8f, 0.8f, 1.0f};
float specularColor[] = {1.0f, 1.0f, 1.0f, 1.0f};
float cR[] = {1.0, 0.0, 0.0, 1.0};
float cG[] = {0.0, 1.0, 0.0, 1.0};
float cB[] = {0.0, 0.0, 1.0, 1.0};
float cY[] = {1.0, 1.0, 0.0, 1.0};

float luzRojo[] = {10.0, 5.0, 0.0f, 1.0f};
float luzVerde[] = {-10.0, 5.0, 0.0f, 1.0f};
float luzAzul[] = {10.0, -5.0, 0.0f, 1.0f};
float luzAmarillo[] = {-10.0, -5.0, 0.0f, 1.0f};

//Sphere
GLUquadricObj *m_Sphere;

void Set_Lights()
{
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, cR);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT0, GL_POSITION, luzRojo);

    glLightfv(GL_LIGHT1, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, cG);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT1, GL_POSITION, luzVerde);

    glLightfv(GL_LIGHT2, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, cB);
    glLightfv(GL_LIGHT2, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT2, GL_POSITION, luzAzul);

    glLightfv(GL_LIGHT3, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT3, GL_DIFFUSE, cY);
    glLightfv(GL_LIGHT3, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT3, GL_POSITION, luzAmarillo);

}

void Draw_Spheres()
{
    for(int y = 0; y < 6; y++)
    {
        for(int x = 0; x < 4; x++)
        {
            glPushMatrix();
                glTranslatef((x/3.0)-0.5, (y/4.5)-0.5, 0.0);
                gluSphere(m_Sphere, 0.1, 40, 40);
            glPopMatrix();
        }
    }
}

void Move_Spheres(float s)
{
    glRotatef(TimeStart*s, 0.0, 1.0, 0.0);
}

void reshape(int w, int h)
{
    H=(h==0)? 1:h;
    W=(w==0)? 1:w;

    glViewport(0, 0, W, H);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    aspect = (float)W/(float)H;

    if(W >= H)
    {
        clipAreaL = -1.0 * aspect;
        clipAreaR = 1.0 * aspect;
        clipAreaB = -1.0;
        clipAreaT = 1.0;
    }
    else
    {
        clipAreaL = -1.0;
        clipAreaR = 1.0;
        clipAreaB = -1.0 / aspect;
        clipAreaT = 1.0 / aspect;
    }
    gluOrtho2D(clipAreaL, clipAreaR, clipAreaB, clipAreaT);

    //gluPerspective(40.0f, (GLfloat)W/(GLfloat)H, 1.0f, 1000.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void inicializar(void)
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    reshape(W, H);
    glEnable(GL_DEPTH_TEST); //Profundidad

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Activate Lights
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);

    //Set Light Colors/Position
    Set_Lights();

    //Sphere
    m_Sphere = gluNewQuadric();

    //Activate Materials
    /*glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);*/
    //---

    TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
    //Time
    TimeStart = glutGet(GLUT_ELAPSED_TIME);
    counter -= deltaTime;

    //Counter reset
    if(counter <=0 ) counter = 1000;

    glutPostRedisplay();
}

void render(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    /*gluLookAt(cubeWorldSize/2, 9.0f, 15.0f,
              cubeWorldSize/2, 3.0f, 4.0f,
              0.0f, 10.0f, 0.0f);*/
    //Camera
    /*glRotatef(cameraAngleX, 1.0, 0.0, 0.0);
    glRotatef(cameraAngleY, 0.0, 1.0, 0.0);*/

    //Draw
    Move_Spheres(speed*3);
    Draw_Spheres();

    //Tiempo
    deltaTime = TimeStart - TimeOld;
    TimeOld = TimeStart;

    glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
    //estados_teclas[key]=true;
    switch(key)
    {
        case 'w':
        case 'W': cameraAngleX --; break;

        case 's':
        case 'S': cameraAngleX ++; break;

        case 'a':
        case 'A': cameraAngleY --; break;

        case 'd':
        case 'D': cameraAngleY ++; break;

            exit(0);
    }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
    //estados_teclas[key]=false;
    switch(key)
    {
        case 'a':
        case 'A':
            //que ahce
            break;
    }
}

void eventosTecladoEspecial(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_F1:
            //que ahce al presionar f1
            break;
    }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_F1:
            //que ahce al presionar f1
            break;
    }
}

/*void analizar_teclas(void)
{
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
}*/

void eventosMouse(int button, int state, int x, int y)
{
    switch(state)
    {
        case GLUT_UP:
            switch(button)
            {
                case GLUT_LEFT_BUTTON:

                    break;
                case GLUT_RIGHT_BUTTON:
                    break;
                case GLUT_MIDDLE_BUTTON:
                    break;
            }
            break;
        case GLUT_DOWN:
            break;
    }
}

int main(int argc, char **argv)
{
    //inicamos el glut
    glutInit(&argc, argv);
    //Especificamos el modo de display
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    //Creamos la ventana
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(10, 150);
    glutCreateWindow("Aplicacion OpenGL con GLUT");

    glutIdleFunc(Idle);
    glutDisplayFunc(render);
    glutReshapeFunc(reshape);
    inicializar();

    ///Teclados y mouse
    //Los que revisan teclado
    glutKeyboardFunc(eventosTeclado);
    glutKeyboardUpFunc(eventosTecladoUp);
    glutSpecialFunc(eventosTecladoEspecial);
    glutSpecialUpFunc(eventosTecladoEspecialUp);
    //mouse
    glutMouseFunc(eventosMouse);

    //El ciclo infinito
    glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

    //Free memory
    if(m_Sphere) gluDeleteQuadric(m_Sphere);
    m_Sphere = NULL;

    return 0;
}

